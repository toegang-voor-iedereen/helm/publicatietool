{{/* vim: set filetype=mustache: */}}
{{/*
Return the proper PHP-FPM image name
*/}}
{{- define "publicatietool.php.image" -}}
{{ include "common.images.image" (dict "imageRoot" .Values.php.image "global" .Values.global) }}
{{- end -}}

{{/*
Return the proper Docker Image Registry Secret Names
*/}}
{{- define "publicatietool.php.imagePullSecrets" -}}
{{ include "common.images.renderPullSecrets" (dict "images" (list .Values.php.image) "context" $) }}
{{- end -}}

{{/*
Compile all warnings into a single message, and call fail.
*/}}
{{- define "publicatietool.php.validateValues" -}}
{{- $messages := list -}}
{{- $messages := append $messages (include "publicatietool.php.validateValues.extraVolumes" .) -}}
{{- $messages := without $messages "" -}}
{{- $message := join "\n" $messages -}}

{{- if $message -}}
{{-   printf "\nVALUES VALIDATION:\n%s" $message | fail -}}
{{- end -}}
{{- end -}}

{{/* Validate values of PHP-FPM - Incorrect extra volume settings */}}
{{- define "publicatietool.php.validateValues.extraVolumes" -}}
{{- if and (.Values.php.extraVolumes) (not .Values.php.extraVolumeMounts) -}}
php: missing-extra-volume-mounts
    You specified extra volumes but not mount points for them. Please set
    the extraVolumeMounts value
{{- end -}}
{{- end -}}

{{/*
 Create the name of the service account to use
 */}}
{{- define "publicatietool.php.serviceAccountName" -}}
{{- if .Values.php.serviceAccount.create -}}
    {{ default (include "common.names.fullname" .) .Values.php.serviceAccount.name }}-php
{{- else -}}
    {{ default "default" .Values.php.serviceAccount.name }}-php
{{- end -}}
{{- end -}}

{{- define "publicatietool.php.env" -}}
- name: APP_ENV
  value: "k8s"
{{- if .Values.smtp.enabled}}
- name: SMTP_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .Values.smtp.password.secretKeyRef.name | quote }}
      key: {{ .Values.smtp.password.secretKeyRef.key | quote }}
{{- end }}
- name: DB_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .Values.database.password.secretKeyRef.name | quote }}
      key: {{ .Values.database.password.secretKeyRef.key | quote }}
- name: SOLR_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .Values.solr.password.secretKeyRef.name | quote }}
      key: {{ .Values.solr.password.secretKeyRef.key | quote }}
- name: RABBITMQ_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .Values.rabbitmq.password.secretKeyRef.name | quote }}
      key: {{ .Values.rabbitmq.password.secretKeyRef.key | quote }}
- name: S3_ACCESS_KEY
  valueFrom:
    secretKeyRef:
      name: {{ .Values.s3.username.secretKeyRef.name | quote }}
      key: {{ .Values.s3.username.secretKeyRef.key | quote }}
- name: S3_SECRET_KEY
  valueFrom:
    secretKeyRef:
      name: {{ .Values.s3.password.secretKeyRef.name | quote }}
      key: {{ .Values.s3.password.secretKeyRef.key | quote }}
{{- if .Values.php.proxy.enabled }}
- name: HTTP_PROXY
  value: {{ .Values.php.proxy.endpoint | quote }}
- name: HTTPS_PROXY
  value: {{ .Values.php.proxy.endpoint | quote }}
- name: NO_PROXY
  value: {{ .Values.php.proxy.noProxy | join "," | quote }}
- name: http_proxy
  value: {{ .Values.php.proxy.endpoint | quote }}
- name: https_proxy
  value: {{ .Values.php.proxy.endpoint | quote }}
- name: no_proxy
  value: {{ .Values.php.proxy.noProxy | join "," | quote }}
{{- end -}}
{{- if .Values.php.extraEnvVars }}
{{- include "common.tplvalues.render" (dict "value" . "context" $) }}
{{- end -}}
{{- end -}}

{{- define "publicatietool.php.volumes" -}}
- name: hash-volume
  secret:
    secretName: {{ .Values.php.secret.salt.name | quote }}
- name: private-volume
  persistentVolumeClaim:
    claimName: "{{ include "common.names.fullname" . }}-private"
- name: tmp-volume
  persistentVolumeClaim:
    claimName: "{{ include "common.names.fullname" . }}-tmp"
- name: public-volume
  persistentVolumeClaim:
    claimName: "{{ include "common.names.fullname" . }}-public"
- name: settings-volume
  configMap:
    name: "{{ include "common.names.fullname" . }}-drupal-settings"
- name: config-split-volume
  configMap:
    name: "{{ include "common.names.fullname" . }}-drupal-config-split"
{{ if .Values.php.extraVolumes }}
{{- include "common.tplvalues.render" (dict "value" .Values.php.extraVolumes "context" $) }}
{{- end -}}
{{- end -}}

{{- define "publicatietool.php.volumeMounts" -}}
- name: hash-volume
  mountPath: "/var/www/app/config/hash"
  readOnly: true
- name: private-volume
  mountPath: "/var/private"
- name: tmp-volume
  mountPath: "/var/www/tmp"
- name: public-volume
  mountPath: "/var/www/app/web/sites/default/files"
- name: settings-volume
  mountPath: "/var/www/app/web/sites/default/settings.php"
  subPath: "settings.php"
  readOnly: true
- name: config-split-volume
  mountPath: "/var/www/app/config/splits/k8s/config_split.patch.search_api.server.solr.yml.php"
  subPath: "config_split.patch.search_api.server.solr.yml.php"
  readOnly: true
- name: config-split-volume
  mountPath: "/var/www/app/config/splits/k8s/config_split.patch.s3fs.settings.yml"
  subPath: "config_split.patch.s3fs.settings.yml"
  readOnly: true
- name: config-split-volume
  mountPath: "/var/www/app/config/splits/k8s/config_split.patch.pt_validation.settings.yml"
  subPath: "config_split.patch.pt_validation.settings.yml"
  readOnly: true
{{- if .Values.nginx.ingress.enabled }}
- name: config-split-volume
  mountPath: "/var/www/app/config/splits/k8s/config_split.patch.simple_sitemap.settings.yml"
  subPath: "config_split.patch.simple_sitemap.settings.yml"
  readOnly: true
{{- end -}}
{{- if .Values.smtp.enabled }}
- name: config-split-volume
  mountPath: "/var/www/app/config/splits/k8s/config_split.patch.symfony_mailer.mailer_transport.smtp.yml.php"
  subPath: "config_split.patch.symfony_mailer.mailer_transport.smtp.yml.php"
  readOnly: true
- name: config-split-volume
  mountPath: "/var/www/app/config/splits/k8s/config_split.patch.system.site.yml"
  subPath: "config_split.patch.system.site.yml"
  readOnly: true
{{- end }}
{{ if .Values.php.extraVolumeMounts }}
{{- include "common.tplvalues.render" (dict "value" .Values.php.extraVolumeMounts "context" $) }}
{{- end -}}
{{- end -}}

{{- define "publicatietool.php.envFrom" -}}
{{- if .Values.php.extraEnvVarsCM }}
- configMapRef:
    name: {{ include "common.tplvalues.render" (dict "value" .Values.php.extraEnvVarsCM "context" $) }}
{{- end }}
{{- if .Values.php.extraEnvVarsSecret }}
- secretRef:
    name: {{ include "common.tplvalues.render" (dict "value" .Values.php.extraEnvVarsSecret "context" $) }}
{{- end }}
{{- end -}}

{{- define "publicatietool.php.sharedSpec" -}}
{{- include "publicatietool.php.imagePullSecrets" . }}
shareProcessNamespace: {{ .Values.php.sidecarSingleProcessNamespace }}
serviceAccountName: {{ template "publicatietool.php.serviceAccountName" . }}
automountServiceAccountToken: {{ .Values.php.automountServiceAccountToken }}
{{- if .Values.php.hostAliases }}
hostAliases: {{- include "common.tplvalues.render" (dict "value" .Values.php.hostAliases "context" $) | nindent 2 }}
{{- end }}
hostNetwork: {{ .Values.php.hostNetwork }}
hostIPC: {{ .Values.php.hostIPC }}
{{- if .Values.php.priorityClassName }}
priorityClassName: {{ .Values.php.priorityClassName | quote }}
{{- end }}
{{- if .Values.php.nodeSelector }}
nodeSelector: {{- include "common.tplvalues.render" (dict "value" .Values.php.nodeSelector "context" $) | nindent 2 }}
{{- end }}
{{- if .Values.php.tolerations }}
tolerations: {{- include "common.tplvalues.render" (dict "value" .Values.php.tolerations "context" $) | nindent 2 }}
{{- end }}
{{- if .Values.php.schedulerName }}
schedulerName: {{ .Values.php.schedulerName | quote }}
{{- end }}
{{- if .Values.php.topologySpreadConstraints }}
topologySpreadConstraints: {{- include "common.tplvalues.render" (dict "value" .Values.php.topologySpreadConstraints "context" .) | nindent 2 }}
{{- end }}
{{- if .Values.php.podSecurityContext.enabled }}
securityContext: {{- omit .Values.php.podSecurityContext "enabled" | toYaml | nindent 2 }}
{{- end }}
volumes:
  {{- include "publicatietool.php.volumes" $ | nindent 2 }}
{{- end -}}

{{- define "publicatietool.php.sharedContainerDefinitions" -}}
image: {{ include "publicatietool.php.image" . }}
imagePullPolicy: {{ .Values.php.image.pullPolicy | quote }}
{{- if .Values.php.containerSecurityContext.enabled }}
securityContext: {{- omit .Values.php.containerSecurityContext "enabled" | toYaml | nindent 2 }}
{{- end }}
env:
  {{- include "publicatietool.php.env" $ | nindent 2 }}
envFrom:
  {{- include "publicatietool.php.envFrom" $ | nindent 2 }}
volumeMounts:
  {{- include "publicatietool.php.volumeMounts" $ | nindent 2 }}
{{- end -}}

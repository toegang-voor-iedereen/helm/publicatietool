---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "common.names.fullname" . }}-drupal-settings
  namespace: {{ .Release.Namespace }}
  labels: {{- include "common.labels.standard" ( dict "customLabels" .Values.commonLabels "context" $ ) | nindent 4 }}
  {{- if .Values.commonAnnotations }}
  annotations: {{- include "common.tplvalues.render" ( dict "value" .Values.commonAnnotations "context" $ ) | nindent 4 }}
  {{- end }}
data:
  settings.php: |
    <?php

    // phpcs:ignoreFile

    $databases = [
      'default' => [
        'default' => [
          'database' => "{{ .Values.database.database }}",
          'username' => "{{ .Values.database.username }}",
          'password' => getenv('DB_PASSWORD'),
          'host' => "{{ .Values.database.host }}",
          'port' => "{{ .Values.database.port }}",
          'driver' => "{{ .Values.database.driver }}",
          'prefix' => getenv('DB_PREFIX') ?: '',
          'collation' => getenv('DB_COLLATION') ?: 'utf8mb4_general_ci',
        ]
      ]
    ];

    $settings['config_sync_directory'] = '../config/sync';
    $settings['hash_salt'] = file_get_contents('../config/hash/salt.txt');
    $settings['update_free_access'] = FALSE;
    $settings['reverse_proxy'] = TRUE;
    $settings['reverse_proxy_addresses'] = [$_SERVER['REMOTE_ADDR']];
    $settings['reverse_proxy_trusted_headers'] = \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_FOR |
      \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_HOST |
      \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_PORT |
      \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_PROTO |
      \Symfony\Component\HttpFoundation\Request::HEADER_FORWARDED;
    $settings['file_private_path'] = '/var/private';
    $settings['file_temp_path'] = '/var/www/tmp';
    $settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';
    $settings['trusted_host_patterns'] = ['{{ .Values.drupal.trustedHostPattern }}'];
    $settings['file_scan_ignore_directories'] = ['node_modules', 'bower_components'];
    $settings['entity_update_batch_size'] = 50;
    $settings['entity_update_backup'] = TRUE;
    $settings['migrate_node_migrate_type_classic'] = FALSE;
    $settings['rabbitmq_credentials']['default'] = [
      'host' => "{{ .Values.rabbitmq.host }}",
      'port' => "{{ .Values.rabbitmq.port }}",
      'vhost' => "{{ .Values.rabbitmq.vhost }}",
      'username' => "{{ .Values.rabbitmq.username }}",
      'password' => getenv('RABBITMQ_PASSWORD') ?: 'rabbitmq',
      'options' => [
        'connection_timeout' => 5,
        'read_write_timeout' => 5,
      ],
    ];
    $settings['queue_service_queue.publicatietool.inbound'] = 'queue.rabbitmq.default';
    $settings['queue_service_queue.publicatietool.outbound'] = 'queue.rabbitmq.default';

    $config['config_split.config_split.dev']['status'] = false;
    $config['config_split.config_split.sdx']['status'] = false;
    $config['config_split.config_split.con']['status'] = false;
    $config['config_split.config_split.qua']['status'] = false;
    $config['config_split.config_split.stg']['status'] = false;
    $config['config_split.config_split.k8s']['status'] = true;

    $config['system.logging']['error_level'] = 'verbose';
    $config['system.performance']['css']['preprocess'] = true;
    $config['system.performance']['js']['preprocess'] = true;

    {{- if .Values.php.proxy.enabled }}
    {{- if .Values.php.proxy.https }}
    $settings['http_client_config']['proxy']['https'] = '{{ .Values.php.proxy.endpoint }}';
    {{- else }}
    $settings['http_client_config']['proxy']['http'] = '{{ .Values.php.proxy.endpoint }}';
    {{- end }}

    $settings['http_client_config']['proxy']['no'] = [
      {{- range .Values.php.proxy.noProxy }}
      {{ . | squote }},
      {{- end }}
    ];
    {{- end }}

    if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
      include $app_root . '/' . $site_path . '/settings.local.php';
    }
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "common.names.fullname" . }}-drupal-config-split
  namespace: {{ .Release.Namespace }}
  labels: {{- include "common.labels.standard" ( dict "customLabels" .Values.commonLabels "context" $ ) | nindent 4 }}
  {{- if .Values.commonAnnotations }}
  annotations: {{- include "common.tplvalues.render" ( dict "value" .Values.commonAnnotations "context" $ ) | nindent 4 }}
  {{- end }}
data:
  "config_split.patch.search_api.server.solr.yml.php": |
    adding:
      backend_config:
        connector_config:
          scheme: http

          # Rendered by Helm
          host: "{{ .Values.solr.host }}"
          port: {{ .Values.solr.port }}
          core: "{{ .Values.solr.collection }}"
          username: "{{ .Values.solr.username }}"

          # Loading this by K8s secret, then running envsubst
          password: "<?= getenv('SOLR_PASSWORD'); ?>"
    removing:
      backend_config:
        connector_config:
          scheme: https
          host: solr.toegangvooriedereen.nl
          port: 443
          username: solr
          password: SolrRocks
          core: production
  "config_split.patch.s3fs.settings.yml": |
    adding:
      bucket: {{ .Values.s3.bucket }}
      hostname: '{{ .Values.s3.protocol }}://{{ .Values.s3.hostname }}:{{ .Values.s3.port }}'
    removing:
      bucket: production-conversion
      hostname: 'http://minio:9000'
  "config_split.patch.pt_validation.settings.yml": |
    adding:
      url: {{ .Values.php.elementStructureConverter.endpoint | squote }}
    removing:
      url: 'http://element-structure-validator:3000/validate'
  {{- if .Values.nginx.ingress.enabled }}
  "config_split.patch.simple_sitemap.settings.yml": |
    adding:
      {{- if .Values.nginx.ingress.tls }}
      base_url: 'https://{{ .Values.nginx.ingress.hostname }}'
      {{- else }}
      base_url: 'http://{{ .Values.nginx.ingress.hostname }}'
      {{- end }}
    removing:
      base_url: 'https://publicatietool.ddev.site'
  {{- end }}
  {{- if .Values.smtp.enabled }}
  "config_split.patch.symfony_mailer.mailer_transport.smtp.yml.php": |
    adding:
      configuration:
        user: {{ .Values.smtp.username | quote }}
        pass: "<?= getenv('SMTP_PASSWORD'); ?>"
        host: {{ .Values.smtp.host | quote }}
        port: {{ .Values.smtp.port }}
        query:
          verify_peer: {{ .Values.smtp.verify_peer | toYaml }}
    removing:
      configuration:
        user: username
        pass: password
        host: localhost
        port: 1025
        query:
          verify_peer: false
  "config_split.patch.system.site.yml": |
    adding:
      name: {{ .Values.smtp.from.name | quote }}
      mail: {{ .Values.smtp.from.address | quote }}
    removing:
      name: NLdoc
      mail: local@nldoc.nl
  {{- end }}
